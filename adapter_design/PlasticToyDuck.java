package adapter_design;

public class PlasticToyDuck implements ToyDuck{
	
	public void squeak()
    {
        System.out.println("Squeak");
    }

}
