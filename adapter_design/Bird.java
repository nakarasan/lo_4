package adapter_design;

public interface Bird {

	public void fly();
    public void makeSound();
	
}
