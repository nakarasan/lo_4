package iterator_design;

public interface Iterator {
	
	boolean hasNext();

    Object next();

}
