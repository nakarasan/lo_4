package iterator_design;

import java.util.Iterator;

public interface Collection {
	
	public NotificationIterator createIterator();

}
