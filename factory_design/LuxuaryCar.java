package factory_design;

public class LuxuaryCar extends Car{
	
	private CarColour colour;

	public LuxuaryCar(CarColour colour) {
		super(CarType.LUXUARY);
		this.colour = colour;
		assemble();
		paint();
	}

	@Override
	void assemble() {
		System.out.println("Assemble Luxuary Car");
		
	}

	@Override
	void paint() {
		System.out.println("Painted colour is " +colour);
		
	}

}
