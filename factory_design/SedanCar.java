package factory_design;

public class SedanCar extends Car {
	
	private CarColour colour;

	public SedanCar(CarColour colour) {
		super(CarType.SEDAN);
		this.colour = colour;
		assemble();
		paint();
	}

	@Override
	void assemble() {
		System.out.println("Assemble Sedan Car");
		
	}

	@Override
	void paint() {
		System.out.println("Painted colour is " +colour);
		
	}

}
