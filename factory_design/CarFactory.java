package factory_design;

public class CarFactory {
	
	public static Car buildCar(CarType type, CarColour colour) {
		
		switch (type) {
		case SMALL:
			return new SmallCar(colour);
		case SEDAN:
			return new SedanCar(colour);
		case LUXUARY:
			return new LuxuaryCar(colour);
		
		}
		return null;
	}

}
