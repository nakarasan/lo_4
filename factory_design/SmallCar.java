package factory_design;

public class SmallCar extends Car{

	private CarColour colour;
	
	public SmallCar(CarColour colour2) {
		super(CarType.SMALL);
		this.colour = colour;
		assemble();
		paint();
	}

	@Override
	void assemble() {
		System.out.println("Assemble Small Car");
		
	}

	@Override
	void paint() {
		System.out.println("Painted colour is " +colour);
		
	}
	

}
